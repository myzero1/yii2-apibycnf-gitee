<?php

namespace myzero1\apibycnf\assets\php;

use yii\web\AssetBundle;

/**
 * Main asset for the `adminlte` theming
 */
class JsonEditorAsset extends AssetBundle
{
    //public $baseUrl = '@web';
    public $css = [
        'jsoneditor-5.32.1/dist/jsoneditor.min.css',
        // 'custom.css',
    ];
    public $js = [
        'jsoneditor-5.32.1/dist/jsoneditor.min.js',
        'apibycnf-ready.js',
        'apibycnf-utils.js',
        'apibycnf-callback.js',
        'apibycnf-init.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];


    /**
     * @inheritdoc
     */
    public function init()
    {
        // $sourcePath = '@vendor/myzero1/yii2-apibycnf-gitee/src/assets/assets';
        $this->sourcePath = dirname(__DIR__, 2) . '/assets/assets';

        parent::init();
    }
}
