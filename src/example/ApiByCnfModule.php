<?php

namespace myzero1\apibycnf\example;

use Yii;
use yii\base\Module as BaseModule;
use yii\base\BootstrapInterface;
use myzero1\apibycnf\components\rest\ApiHelper;

/**
 * example module definition class
 */
class ApiByCnfModule extends BaseModule implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'example\controllers';
    public $apiTokenExpire = 86400; // 24h
    public $captchaExpire = 60 * 5; // 5m
    public $captchaMaxTimes = 3;
    public $runningAsDocActions = ['*' => '*']; // all action
    public $fixedUser = ['id' => 1, 'username' => 'myzero1', 'api_token' => 'myzero1ApiToken'];
    public $smsAndCacheComponents = [
        'captchaCache' => [
            'class' => '\yii\caching\FileCache',
            'cachePath' => '@runtime/captchaCache',
        ],
        'captchaSms' => [
            // 腾讯云
            'class' => 'myzero1\smser\QcloudsmsSmser',
            'appid' => '140028081944', // appid
            'appkey' => '23e167badfc804d97d454e32e258b7833', // 请替换成您的apikey
            'smsSign' => '玩索得',
            'expire' => '5', //分钟
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function bootstrap($app)
    {
        if ($app instanceof \yii\web\Application) {
            Yii::$app->params['restbyconfAuthenticator_4b86d70ad19b58abaa9562577c093d9f'] = 'queryParamAuth';
            Yii::$app->params['restbyconfUnAuthenticateActions_4b86d70ad19b58abaa9562577c093d9f'] = [
                'post /z1authenticator/login',
                'post /z1authenticator/join',
                'post /z1tools/captcha',
            ];
            $apiUrlRules = ApiHelper::getApiUrlRules($this->id);
            $app->getUrlManager()->addRules($apiUrlRules, $append = true);
        }

        // Yii::setAlias('@example', '@vendor/myzero1/yii2-apibycnf-gitee1/src/example');
        Yii::setAlias('@example', __DIR__);
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
        $this->response->on(\yii\web\Response::EVENT_BEFORE_SEND, function ($event) {
            $res = $event->sender;
            if (!$res->isSuccessful && is_array($res->data)) {
                if ($res->data['code'] == 0) {
                    $res->data['code'] = '735' . $res->data['status'];
                }

                $res->data = [
                    'code' => $res->data['code'],
                    'msg' => $res->data['name'],
                    'data' => [
                        'message' => $res->data['data']['message'],
                        'status' => $res->data['data']['status'],
                        'type' => $res->data['data']['type'],
                    ],
                ];
            }
        });
    }
}
