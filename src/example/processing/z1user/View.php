<?php

/**
 * @link https://github.com/myzero1
 * @copyright Copyright (c) 2019- My zero one
 * @license https://github.com/myzero1/yii2-apibycnf/blob/master/LICENSE
 */

namespace example\processing\z1user;

use Yii;
use yii\db\Query;
use yii\web\ServerErrorHttpException;
use myzero1\apibycnf\components\rest\ApiHelper;
use myzero1\apibycnf\components\rest\ApiCodeMsg;
use myzero1\apibycnf\components\rest\ApiActionProcessing;
use example\processing\z1user\io\ViewIo as Io;

/**
 * implement the ActionProcessing
 *
 * For more details and usage information on CreateAction, see the [guide article](https://github.com/myzero1/yii2-apibycnf).
 *
 * @author Myzero1 <myzero1@sina.com>
 * @since 0.0
 */
class View implements ApiActionProcessing
{
    /**
     * @param $params mixed
     * @return array date will return to create action.
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function processing($params = null)
    {
        return ApiHelper::processing($this, Io::class);
    }

    /**
     * @param  array $input from the request body
     * @return array
     */
    public function inputValidate($input)
    {
        return Io::inputValidate($input);
    }

    /**
     * @param  array $validatedInput validated data
     * @return array
     */
    public function mappingInput2db($validatedInput)
    {
        $inputFieldMap = [
            'demo_name' => 'name735',
            'demo_description' => 'description735',
        ];
        $in2dbData = ApiHelper::input2DbField($validatedInput, $inputFieldMap);

        return $in2dbData;
    }

    /**
     * @param  array $in2dbData mapped data form input
     * @return array
     */
    public function completeData($in2dbData)
    {
        $in2dbData['updated_at'] = $in2dbData['created_at'] = time();
        $in2dbData = ApiHelper::inputFilter($in2dbData); // You should comment it, when in search action.
        return $in2dbData;
    }

    /**
     * @param  array $completedData completed data
     * @return array
     * @throws ServerErrorHttpException
     */
    public function handling($completedData)
    {
        $result = (new Query())
            ->from('z1_user t')
            // ->groupBy(['t.id'])
            // ->join('INNER JOIN', 'info i', 'i.user_id = t.id')
            ->andFilterWhere([
                'and',
                ['=', 't.id', $completedData['id']],
            ])
            ->select([
                't.id',
            ])
            ->one();

        if (!$result) {
            $result = [
                'code' => ApiCodeMsg::NOT_FOUND,
                'msg' => ApiCodeMsg::NOT_FOUND_MSG,
                'data' => new \StdClass(),
            ];
        }

        return $result;
    }

    /**
     * @param  array $savedData saved data
     * @return array
     */
    public function mappingDb2output($handledData)
    {
        $outputFieldMap = [
            'name735' => 'demo_name',
            'description735' => 'demo_description',
        ];
        $db2outData = ApiHelper::db2OutputField($handledData, $outputFieldMap);

        // $db2outData['created_at'] = ApiHelper::time2string($db2outData['created_at']);
        // $db2outData['updated_at'] = ApiHelper::time2string($db2outData['updated_at']);

        return $db2outData;
    }

    /**
     * @param  array $db2outData completed data form database
     * @return array
     */
    public function completeResult($db2outData = [])
    {
        return ApiHelper::completeResult($db2outData);
    }

    /**
     * @return array
     */
    public function egOutputData()
    {
        return Io::egOutputData(); // for demo
    }
}
