<?php

/**
 * @link https://github.com/myzero1
 * @copyright Copyright (c) 2019- My zero one
 * @license https://github.com/myzero1/yii2-apibycnf/blob/master/LICENSE
 */

namespace example\processing\z1tools;

use Yii;
use yii\db\Query;
use yii\web\ServerErrorHttpException;
use myzero1\apibycnf\components\rest\ApiHelper;
use myzero1\apibycnf\components\rest\ApiCodeMsg;
use myzero1\apibycnf\components\rest\ApiActionProcessing;
use example\processing\z1tools\io\UploadIo as Io;

/**
 * implement the ActionProcessing
 *
 * For more details and usage information on CreateAction, see the [guide article](https://github.com/myzero1/yii2-apibycnf).
 *
 * @author Myzero1 <myzero1@sina.com>
 * @since 0.0
 */
class Upload implements ApiActionProcessing
{
    /**
     * @param $params mixed
     * @return array date will return to create action.
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function processing($params = null)
    {
        return ApiHelper::processing($this, Io::class);
    }

    /**
     * @param  array $input from the request body
     * @return array
     */
    public function inputValidate($input)
    {
        $input['post']['file'] = 'file placeholder';
        return Io::inputValidate($input);
    }

    /**
     * @param  array $validatedInput validated data
     * @return array
     */
    public function mappingInput2db($validatedInput)
    {
        $inputFieldMap = [
            'demo_name' => 'name735',
            'demo_description' => 'description735',
        ];
        $in2dbData = ApiHelper::input2DbField($validatedInput, $inputFieldMap);

        return $in2dbData;
    }

    /**
     * @param  array $in2dbData mapped data form input
     * @return array
     */
    public function completeData($in2dbData)
    {
        $in2dbData['updated_at'] = $in2dbData['created_at'] = time();
        $in2dbData = ApiHelper::inputFilter($in2dbData); // You should comment it, when in search action.
        return $in2dbData;
    }

    /**
     * @param  array $completedData completed data
     * @return array
     * @throws ServerErrorHttpException
     */
    public function handling($completedData)
    {
        $file = \yii\web\UploadedFile::getInstanceByName('file');
        $newDirectory = $completedData['directory'];
        \yii\helpers\BaseFileHelper::createDirectory($newDirectory);
        $newName = sprintf('%s/%s-%s.%s', $newDirectory, $file->baseName, time(), $file->extension);
        $extensions = explode(',', $completedData['extension']);
        if (!in_array($file->extension, $extensions)) {
            return [
                'code' => '735400',
                'msg' => '输入参数验证错误',
                'data' => [
                    "file" => [
                        sprintf('只允许上传后缀为%s的文件', $completedData['extension'])
                    ]
                ],
            ];
        }

        $ignore = sprintf('%s/.gitignore', $newDirectory);

        file_put_contents($ignore, "*\n!.gitignore");
        $file->saveAs($newName);

        return [
            'absUrl' => Yii::$app->urlManager->createAbsoluteUrl([$newName]),
            'relUrl' => '/' . $newName,
        ];
    }

    /**
     * @param  array $savedData saved data
     * @return array
     */
    public function mappingDb2output($handledData)
    {
        $outputFieldMap = [
            'name735' => 'demo_name',
            'description735' => 'demo_description',
        ];
        $db2outData = ApiHelper::db2OutputField($handledData, $outputFieldMap);

        // $db2outData['created_at'] = ApiHelper::time2string($db2outData['created_at']);
        // $db2outData['updated_at'] = ApiHelper::time2string($db2outData['updated_at']);

        return $db2outData;
    }

    /**
     * @param  array $db2outData completed data form database
     * @return array
     */
    public function completeResult($db2outData = [])
    {
        return ApiHelper::completeResult($db2outData);
    }

    /**
     * @return array
     */
    public function egOutputData()
    {
        return Io::egOutputData(); // for demo
    }
}
