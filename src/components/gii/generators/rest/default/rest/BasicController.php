<?php
echo "<?php\n";
?>

/**
 * @link https://github.com/myzero1
 * @copyright Copyright (c) 2019- My zero one
 * @license https://github.com/myzero1/yii2-apibycnf/blob/master/LICENSE
 */

namespace <?= $generator->confAarray['json']['restModuleAlias'] . '\controllers' ?>;

use \myzero1\apibycnf\components\rest\ApiController;

/**
 * BasicController implements the CRUDI actions for the module.
 */
class BasicController extends ApiController
{
    public function beforeAction($action)
    {
        $token = \Yii::$app->request->get('token');
        \Yii::$app->user->identity = \Yii::$app->user->identityClass::findIdentityByAccessToken($token);

        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        return $behaviors;
    }
}