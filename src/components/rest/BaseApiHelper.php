<?php

/**
 * @link https://github.com/myzero1
 * @copyright Copyright (c) 2019- My zero one
 * @license https://github.com/myzero1/yii2-apibycnf/blob/master/LICENSE
 */

namespace myzero1\apibycnf\components\rest;

use yii\web\ServerErrorHttpException;
use myzero1\apibycnf\components\rest\Helper;
use myzero1\apibycnf\components\rest\ApiCodeMsg;
use myzero1\apibycnf\components\rest\HandlingHelper;
use Yii;

/**
 * Some Helpful function
 *
 * For more details and usage information on CreateAction, see the [guide article](https://github.com/myzero1/yii2-apibycnf).
 *
 * @author Myzero1 <myzero1@sina.com>
 * @since 0.0
 */
class BaseApiHelper
{
    const EXPORT_PAGE = 1;
    const EXPORT_PAGE_SIZE = 999999;
    const DEFAULT_PAGE = 1;
    const DEFAULT_PAGE_SIZE = 30;

    public static function response($data, $code = 0, $msg = '')
    {
        \Yii::$app->response->data = $data;

        if ($code !== 0) {
            \Yii::$app->response->statusCode = $code;
        }

        if ($msg !== '') {
            \Yii::$app->response->statusText = $msg;
        }

        return 0;
    }

    /**
     * @param array $errors ['inputField' => 'inputValue]
     * @return string
     */
    public static function getErrorMsg($errors)
    {
        $error = array_shift($errors);
        $errStr = $error[0];
        $err = explode(':', $errStr);
        return array_pop($err);
    }

    /**
     * @param array $input ['inputField' => 'inputValue]
     * @param array $inputFieldMap ['inputField' => 'dbField]
     * @return array ['dbField' => 'inputValue]
     */
    public static function input2DbField($input, $inputFieldMap = [])
    {
        if (count($inputFieldMap) === 0) {
            return $input;
        } else {
            foreach ($input as $k => $v) {
                if (isset($inputFieldMap[$k])) {
                    $input[$inputFieldMap[$k]] = $v;
                    unset($input[$k]);
                }
            }

            return $input;
        }
    }

    /**
     * @param array ['dbField' => 'inputValue]
     * @param array $outputFieldMap ['inputField' => 'dbField]
     * @return array ['outputField' => 'inputValue]
     */
    public static function db2OutputField($db, $outputFieldMap = [])
    {
        if (count($outputFieldMap) === 0) {
            return $db;
        } else {
            foreach ($db as $k => $v) {
                if (isset($outputFieldMap[$k])) {
                    $db[$outputFieldMap[$k]] = $v;
                    unset($db[$k]);
                }
            }

            return $db;
        }
    }

    /**
     * @param int $timestamp 1552886962 the will be converted timestamp.
     * @return string 2019-03-23
     */
    public static function time2string($timestamp)
    {
        if (empty($timestamp)) {
            return '';
        } else {
            if (self::isTimestamp($timestamp)) {
                return date('Y-m-d H:i:s', $timestamp);
            } else {
                return '';
            }
        }
    }

    /**
     * @param string $string 2019-03-23
     * @return int 1552886962
     */
    public static function string2time($string)
    {
        if (empty($string)) {
            return '';
        } else {
            return strtotime($string);
        }
    }

    /**
     * @param mixed $timestamp .
     * @return bool
     */
    public static function isTimestamp($timestamp)
    {
        $timestamp = intval($timestamp);

        if (strtotime(date('Y-m-d H:i:s', $timestamp)) === $timestamp) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param mixed $data .
     * @return bool
     */
    public static function isReturning($data)
    {
        if (isset($data['code']) && isset($data['msg']) && isset($data['data']) && (count($data) === 3)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @param string $sort +id,-id
     * @param string $defaultSort +id
     * @param array $sortFiledArray ['id','name']
     * @return mixed false | ['sort'=>SORT_ASC, 'sortFile'=>'id]
     */
    public static function getSort($sort, $sortFiledArray, $defaultSort)
    {
        $checkedSort = self::checkSortInput($sort);

        if ($checkedSort !== false) {
            if (in_array($checkedSort['sortFiled'], $sortFiledArray)) {
                return self::decodeSort($checkedSort);
            } else {
                $checkedSortDefault = self::checkSortInput($defaultSort);
                if ($checkedSortDefault !== false) {
                    if (in_array($checkedSortDefault['sortFiled'], $sortFiledArray)) {
                        return self::decodeSort($checkedSortDefault);
                    } else {
                        throw new ServerErrorHttpException('Failed to get sort for checkedSortDefault reason.');
                    }
                } else {
                    throw new ServerErrorHttpException('Failed to get sort for checkedSortDefault reason.');
                }
            }
        } else {
            $checkedSortDefault = self::checkSortInput($defaultSort);
            if ($checkedSortDefault !== false) {
                if (in_array($checkedSortDefault['sortFiled'], $sortFiledArray)) {
                    return self::decodeSort($checkedSortDefault);
                } else {
                    throw new ServerErrorHttpException('Failed to get sort for not a sort field reason.');
                }
            } else {
                throw new ServerErrorHttpException('Failed to get sort for checkedSortDefault reason.');
            }
        }
    }

    /**
     * @param array ['sort'=>'+', 'sortFiled'=>'id']
     * @return array
     */
    public static function decodeSort($sort)
    {
        $sortSetting = [
            '+' => SORT_ASC,
            '-' => SORT_DESC,
        ];

        return [
            'sort' => $sortSetting[$sort['sort']],
            'sortFiled' => $sort['sortFiled'],
        ];
    }

    /**
     * @param string $sort +id,+ id
     * @return mixed false | +id
     */
    public static function checkSortInput($sort)
    {
        $trimSort = trim($sort);
        $pre = substr($trimSort, 0, 1);
        $sortFiled = substr($trimSort, 1);
        $sortFiled = trim($sortFiled);

        if (!empty($pre) && !empty($sortFiled)) {
            if (in_array($pre, ['+', '-'])) {
                return [
                    'sort' => $pre,
                    'sortFiled' => $sortFiled,
                ];
            } else {
                return [
                    'sort' => '+',
                    'sortFiled' => $trimSort,
                ];
            }
        } else {
            return false;
        }
    }

    /**
     * @param string $uncamelized_words
     * @param string $separator
     * @return string
     */
    public static function camelize($uncamelized_words, $separator = '_')
    {
        $uncamelized_words = $separator . str_replace($separator, " ", strtolower($uncamelized_words));
        return ltrim(str_replace(" ", "", ucwords($uncamelized_words)), $separator);
    }

    /**
     * @param string $camelCaps
     * @param string $separator
     * @return string
     */
    public static function uncamelize($camelCaps, $separator = '_')
    {
        return strtolower(preg_replace('/([a-z])([A-Z])/', "$1" . $separator . "$2", $camelCaps));
    }

    /**
     * @param array $urlRule
     * @return array
     */
    public static function optimizeRestUrlRules($urlRules)
    {
        // var_dump($urlRules);exit;
        $apiRuleConfigs = [];
        foreach ($urlRules as $k => $v) {
            if (isset($v['class']) && $v['class'] == '\yii\rest\UrlRule') {
                $tmp = [];
                $tmp['controller'] = $v['controller'];
                if (isset($v['extraPatterns'])) {
                    $tmp['extraPatterns'] = $v['extraPatterns'];
                }

                $apiRuleConfigs[] = $tmp;
            }
        }

        $apiRuleConfigsDealed = [];
        foreach ($apiRuleConfigs as $key => $value) {
            $apiRuleConfigsDealed[] = self::addOptionsAction($value);
        }
        // var_dump($apiRuleConfigsDealed);exit;
        return $apiRuleConfigsDealed;
    }

    /**
     * @param array $urlRule
     * @return array
     */
    public static function addOptionsAction($urlRule)
    {
        $unit = $urlRule;

        if ($urlRule['controller'][0] == 'restbyconf_custom_rules') {
            foreach ($urlRule['extraPatterns'] as $key => $val) {
                if (!is_numeric(strpos($key, 'OPTIONS'))) {
                    //判断是否有空格符
                    if (is_numeric(strpos($key, ' '))) {
                        //存在
                        $tmp = explode(' ', $key);
                        $k = str_replace($tmp[0], 'OPTIONS', $key);
                        $urlRule['extraPatterns'][$k] = 'options';
                    } else {
                        //不存在
                        $urlRule['extraPatterns']['OPTIONS'] = 'options';
                    }
                }
            }

            return $urlRule['extraPatterns'];
        } else {
            //防止默认options控制器被屏蔽
            if (isset($unit['only']) && !empty($unit['only']) && !in_array('options', $unit['only'])) {
                $urlRule['only'][] = 'options';
            }
            if (isset($unit['except']) && !empty($unit['except']) && in_array('options', $unit['except'])) {
                $urlRule['except'] = array_merge(array_diff($unit['except'], ['options']));
            }
            //由于ajax设置请求头后,会有一次options请求,默认为所有路由添加支持options请求
            if (isset($unit['extraPatterns']) && !empty($unit['extraPatterns'])) {
                foreach ($unit['extraPatterns'] as $key => $val) {
                    if (!is_numeric(strpos($key, 'OPTIONS'))) {
                        //判断是否有空格符
                        if (is_numeric(strpos($key, ' '))) {
                            //存在
                            $tmp = explode(' ', $key);
                            $k = str_replace($tmp[0], 'OPTIONS', $key);
                            $urlRule['extraPatterns'][$k] = 'options';
                        } else {
                            //不存在
                            $urlRule['extraPatterns']['OPTIONS'] = 'options';
                        }
                    }
                }
            }
            if (isset($unit['patterns']) && !empty($unit['patterns'])) {
                foreach ($unit['patterns'] as $key => $val) {
                    if (!is_numeric(strpos($key, 'OPTIONS'))) {
                        //判断是否有空格符
                        if (is_numeric(strpos($key, ' '))) {
                            //存在
                            $tmp = explode(' ', $key);
                            $k = str_replace($tmp[0], 'OPTIONS', $key);
                            $urlRule['patterns'][$k] = 'options';
                        } else {
                            //不存在
                            $urlRule['patterns']['OPTIONS'] = 'options';
                        }
                    }
                }
            }

            $config = [
                'class' => '\yii\rest\UrlRule',
                'pluralize' => false,
                'tokens' => [
                    '{id}' => '<id:\\w[\\w,]*>',
                ],
            ];

            return array_merge($config, $urlRule);
        }
    }

    /**
     * @param array $node
     * @return array
     */
    public static function rmNode($node)
    {
        $rmNode = ['node_id', 'add_item_click_before_icon'];
        foreach ($rmNode as $k => $v) {
            unset($node[$v]);
        }

        return $node;
    }

    /**
     * @param string $moduleId
     * @return array
     */
    public static function getApiConf($moduleId)
    {
        if ($moduleId) {
            $path = self::getModulePath($moduleId);
        } else {
            $path = '';
        }
        $confDataPathTmp = sprintf('%s/config/conf_admin.json', $path);
        // $confDataPathDefault = Yii::getAlias('@vendor/myzero1/yii2-apibycnf-gitee/src/components/conf/conf.json');
        $confDataPathDefault = dirname(__DIR__, 2) . '/components/conf/conf.json';

        if (is_file($confDataPathTmp)) {
            $confDataTmp = file_get_contents($confDataPathTmp);
            if (empty($confDataTmp)) {
                $confDataInit = file_get_contents($confDataPathDefault);
            } else {
                $configAdmin = json_decode($confDataTmp, true);
                $configAdminObj = json_decode($confDataTmp);
                $member = $configAdmin['json']['myGroup']['member'];

                $adminControllers = array_keys($configAdmin['json']['controllers']);
                foreach ($adminControllers as $k2 => $v2) {
                    $configAdmin['json']['controllers'][$v2] = $configAdminObj->json->controllers->$v2;
                }

                foreach (array_keys($member) as $k => $v) {
                    $fileName = sprintf('%s/config/conf_%s.json', $path, $v);
                    if (is_file($fileName)) {
                        $data = file_get_contents($fileName);
                        $dataArray = json_decode($data, true);
                        $dataObj = json_decode($data);

                        $controllers = explode(',', $member[$v]);

                        foreach ($controllers as $k1 => $v1) {
                            if (isset($dataArray['json']['controllers'][$v1])) {
                                $configAdmin['json']['controllers'][$v1] = $dataObj->json->controllers->$v1;
                            }
                        }
                    }
                }

                $members = array_keys($configAdmin['json']['myGroup']['member']);
                $members = array_merge(['admin'], $members);
                $configAdmin['schemaRefs']['schema']['properties']['myGroup']['properties']['currentUser']['enum'] = $members;
                // var_dump($configAdmin);exit;
                $confDataInit = json_encode($configAdmin);
            }
        } else {
            $confDataInit = file_get_contents($confDataPathDefault);
        }

        return $confDataInit;
    }

    /**
     * @param string $moduleId
     * @return array
     */
    public static function getApiUrlRules($moduleId)
    {
        if ($moduleId) {
            $path = self::getModulePath($moduleId);
        } else {
            $path = '';
        }
        $confDataPathTmp = sprintf('%s/config/apiUrlRules.php', $path);
        // $confDataPathDefault = Yii::getAlias('@vendor/myzero1/yii2-apibycnf-gitee/src/components/conf/apiUrlRules.php');
        $confDataPathDefault = dirname(__DIR__, 2) . '/components/conf/apiUrlRules.php';

        if (is_file($confDataPathTmp)) {
            $confDataTmp = require $confDataPathTmp;
            if (empty($confDataTmp)) {
                $confDataInit = require $confDataPathDefault;
            } else {
                $confDataInit = $confDataTmp;
            }
        } else {
            $confDataInit = require $confDataPathDefault;
        }

        return $confDataInit;
    }

    /**
     * @param  string $modelClass
     * @param  int $id
     * @param  array $where
     * @return mixed
     */
    public static function findModel($modelClass, $id, $where = [])
    {
        if (count($where)) {
            $model = $modelClass::find()->where($where)->one();
        } else {
            $model = $modelClass::find()->where(['id' => $id, 'is_del' => 1])->one();
        }

        if (!$model) {
            /*
            return [
                'code' => ApiCodeMsg::NOT_FOUND,
                'msg' => ApiCodeMsg::NOT_FOUND_MSG,
                'data' => new \StdClass(),
            ];
            */

            $data = [
                'code' => ApiCodeMsg::NOT_FOUND,
                'msg' => ApiCodeMsg::NOT_FOUND_MSG,
                'data' => new \StdClass(),
            ];

            Yii::$app->response->data = $data;
            Yii::$app->response->send();
            exit;
        } else {
            return $model;
        }
    }

    /**
     * @param array $input
     * @return mixed
     */
    public static function inputFilter($input)
    {
        return array_filter(
            $input,
            function ($v) {
                return !in_array(
                    $v,
                    $invalidParams = [
                        '',
                        null,
                        [],
                    ],
                    true
                );
            }
        );
    }

    /**
     * @param  array $validatedInput
     * @return array
     */
    public static function getPagination($validatedInput)
    {
        $pagination = [];
        if (isset($validatedInput['page']) && !empty($validatedInput['page'])) {
            $pagination['page'] = $validatedInput['page'];
        } else {
            $pagination['page'] = ApiHelper::DEFAULT_PAGE;
        }
        if (isset($validatedInput['page_size']) && !empty($validatedInput['page_size'])) {
            $pagination['page_size'] = $validatedInput['page_size'];
        } else {
            $pagination['page_size'] = ApiHelper::DEFAULT_PAGE_SIZE;
        }

        return $pagination;
    }

    /**
     * @param string $filename export-files
     * @param array $exportParams [
     * 'dataProvider' => $dataProvider,
     * 'columns' => [
     * [
     * 'attribute' => 'id',
     * 'label' => '网吧编码',
     * ],
     * [
     * 'attribute' => 'name',
     * 'label' => '网吧名称',
     * ],
     * ],
     * ]
     * @throws  \RuntimeException
     * @return bool true
     */
    public static function createXls($filename, $exportParams)
    {
        $filenameXls = $filename . '.xls';
        try {
            $exporter = new \yii2tech\spreadsheet\Spreadsheet($exportParams);
        } catch (\Exception $e) {
            throw new \RuntimeException($e);
        }
        $exporter->save($filenameXls);

        return true;
    }

    /**
     * Get the module's name of apibycnf.
     *
     * 调用实例：Helper::
     *
     * @param   void
     * @return  string
     **/
    public static function getRestModuleName()
    {
        foreach (\Yii::$app->modules as $key => $value) {
            if (!is_array($value)) {
                if (is_object($value)) {
                    if ('myzero1\apibycnf\Module' == $value::className()) {
                        return $key;
                    }
                } else {
                    if ('myzero1\apibycnf\Module' == $value) {
                        return $key;
                    }
                }
            }
        }

        return 'RestbyconfModule';
    }

    /**
     * Get the module's id of apibycnf.
     *
     * 调用实例：Helper::
     *
     * @param   void
     * @return  string
     **/
    public static function getApiByCnfModuleId()
    {
        $moduleId = [];
        foreach (\Yii::$app->modules as $key => $value) {
            if (!is_array($value)) {
                if (is_object($value)) {
                    if (stripos($value::className(), "ApiByCnfModule") !== false) {
                        $moduleId[] = $key;
                    }
                }
            }
        }
        return $moduleId;
    }

    /**
     * Get the module's name of apibycnf.
     *
     * 调用实例：Helper::
     *
     * @param   obj $generator
     * @return  bool
     **/
    public static function isRestGenerator($generator)
    {
        $attributes = $generator->attributes;
        $restAttributes = ['conf', 'position', 'confAarray', 'moduleClass', 'moduleID', 'controller', 'action', 'controllerV',];

        foreach ($restAttributes as $k => $v) {
            if (in_array($v, $attributes)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get the module's name of apibycnf.
     *
     * 调用实例：Helper::
     *
     * @param   obj $model
     * @param   int $code
     * @return  array
     **/
    public static function getModelError($model, $code)
    {
        $errors = $model->errors;
        $errorsA = [];
        foreach ($errors as $k => $v) {
            // $errorsA[] = sprintf('`%s`:%s', $k, $v[0]);
            $errorsA[] = $v[0];
        }
        $errorsStr = implode(';', $errorsA);
        return [
            'code' => $code,
            'msg' => self::getErrorMsg($errors),
            'data' => [
                'errStr' => $errorsStr,
                'err' => $errors,
            ],
        ];
    }

    /**
     * @param   string $className
     * @return  string
     **/
    public static function getClassPath($className = 'myzero1\apibycnf\Module')
    {
        $reflection = new \ReflectionClass($className);
        $fileName = $reflection->getFileName();
        return $fileName;
    }

    /**
     * @param   int $moduleId
     * @return  string
     **/
    public static function getModuleClass($moduleId, $setDefault = false)
    {
        if (isset(Yii::$app->modules[$moduleId])) {
            if (is_object(Yii::$app->modules[$moduleId])) {
                return Yii::$app->modules[$moduleId]->className();
            } else {
                return Yii::$app->modules[$moduleId];
            }
        } else {
            if ($setDefault) {
                return sprintf('app\modules\%s\%s', $moduleId, 'ApiByCnfModule');
            } else {
                self::throwError(sprintf('Not found module "%s"', $moduleId), __FILE__, __LINE__);
            }
        }
    }

    /**
     * @param   string $msg
     * @param   string $filePath
     * @param   string $lineNum
     **/
    public static function throwError($msg, $filePath, $lineNum)
    {
        if (defined('YII_ENV') && YII_ENV == 'dev') {
            $fileMsg = sprintf('in file:%s', $filePath);
            $lineMsg = sprintf('on file:%s', $lineNum);
            $msgs = "{$msg}\n{$fileMsg}\n{$lineMsg}";
        } else {
            $msgs = $msg;
        }

        throw new ServerErrorHttpException($msgs);
    }

    /**
     * @param   string $moduleId
     * @return  string
     **/
    public static function getModulePath($moduleId, $confJson = [])
    {
        $moduleClass = self::getModuleClass($moduleId, true);

        if (strpos($moduleClass, 'myzero1\apibycnf') === 0) {
            $tmp = str_replace('myzero1\apibycnf', '', $moduleClass);
            $tmp = str_replace('\\', '/', $tmp);
            // $tmp = Yii::getAlias('@vendor/myzero1/yii2-apibycnf-gitee/src') . $tmp;
            $tmp = dirname(__DIR__, 2)  . $tmp;

            $moduleClassPath = $tmp . '.php';
        } else {
            $moduleClassPath = Yii::getAlias(str_replace('\\', '/', '@' . $moduleClass)) . '.php';
        }




        if (is_file($moduleClassPath)) {
            // if (class_exists($moduleClass)) {
            // $moduleFilePath = self::getClassPath($moduleClass);
            $moduleFilePath = $moduleClassPath;
            $modulePath = dirname($moduleFilePath);
        } else {
            // $modulePath = sprintf('%s/modules/%s', Yii::getAlias('@app'), $moduleId);

            $restModuleName = $confJson['restModuleName'];
            $restModuleAlias = $confJson['restModuleAlias'];
            $restModuleAliasPath = $confJson['restModuleAliasPath'];

            $modulePath = Yii::getAlias($restModuleAliasPath);
            $modulePath = str_replace($restModuleName, $restModuleAlias, $modulePath);
        }

        return $modulePath;
    }

    /**
     * @param   array $array
     * @param   mixed $key
     * @param   mixed $defafult
     * @return  mixed
     **/
    public static function getArrayVal($array, $key, $defafult = '')
    {
        $result = '';
        if (isset($array[$key])) {
            $result = $array[$key];
        } else {
            $result = $defafult;
        }

        return $result;
    }


    /**
     * get Dictionary lable
     * @param string $type
     * @param string $item
     * @param mixed $customDic
     * @return string
     */
    public static function getDictionaryLable($type, $item, $customDic = false, $dicFilePath = false)
    {
        if ($customDic) {
            $dictionary['customDic'] = $customDic;
            $type = 'customDic';
        } else if ($dicFilePath) {
            // $dicFilePath = sprintf('%s/../common/config/dictionary.php', Yii::getAlias('@app'));
            $dictionary = require $dicFilePath;
        } else {
            return '';
        }

        if (isset($dictionary[$type][$item])) {
            return $dictionary[$type][$item];
        } else {
            return '';
        }
    }

    /**
     * filter EgOutputData
     * @param string $type
     * @param string $item
     * @param mixed $customDic
     * @return string
     */
    public static function saveConfJsonStr($confJsonStr, $moduleId)
    {
        $confJson = json_decode($confJsonStr, true);
        $confJsonObj = json_decode($confJsonStr);
        $members = $confJson['json']['myGroup']['member'];
        $currentUser = $confJson['json']['myGroup']['currentUser'];
        $members = array_merge(['admin'], $members);
        $userControllers = [];
        $controllers = [];
        foreach ($members as $k => $v) {
            $userControllers[$k] = explode(',', $v);
            $controllers = array_merge($controllers, $userControllers[$k]);
        }
        $oldControllers = array_keys($confJson['json']['controllers']);
        $adminControllers = array_diff($oldControllers, $controllers);
        $adminControllersObj = [];
        $userControllersObj = [];
        foreach ($oldControllers as $k => $v) {
            if (in_array($v, $adminControllers)) {
                $adminControllersObj[$v] = $confJsonObj->json->controllers->$v;
            } else if (isset($userControllers[$currentUser]) && in_array($v, $userControllers[$currentUser])) {
                $userControllersObj[$v] = $confJsonObj->json->controllers->$v;
            }
        }

        $confJsonObj->schemaRefs->schema->properties->myGroup->properties->currentUser->enum = $members;

        $currentPath = sprintf(
            '%s/config/conf_%s.json',
            ApiHelper::getModulePath(
                $moduleId,
                json_decode($confJsonStr, true)['json']
            ),
            json_decode($confJsonStr, true)['json']['myGroup']['currentUser']
        );

        $currentConfStr = '';
        if ($currentUser == 'admin') {
            $currentConfStr = $confJsonStr;
        } else {
            if (is_file($currentPath)) {
                $currentConfStr = file_get_contents($currentPath);
            } else {
                $currentConfStr = $confJsonStr;
            }

            $currentConfObj = json_decode($currentConfStr, true);
            $currentConfObjTmp['json']['controllers'] = $currentConfObj['json']['controllers'];
            $currentConfStr = json_encode($currentConfObjTmp);
        }

        $currentConfObj = json_decode($currentConfStr);

        if ($currentUser == 'admin') {
            $currentConfObj->json->controllers = $adminControllersObj;
        } else {
            $currentConfObj->json->controllers = $userControllersObj;
        }

        $currentConfStrEnd = json_encode($currentConfObj, JSON_UNESCAPED_UNICODE + JSON_PRETTY_PRINT);

        return $currentConfStrEnd;
    }

    /**
     * filter EgOutputData
     * @param string $type
     * @param string $item
     * @param mixed $customDic
     * @return string
     */
    public static function filterEgOutputData($egOutputDataEncode)
    {
        $tmp =  unserialize($egOutputDataEncode);

        foreach ($tmp as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k1 => $v1) {
                    if (is_array($v1)) {
                        foreach ($v1 as $k2 => $v2) {
                            if (is_string($v2)) {
                                $t2 = explode('---', $v2);
                                $tmp[$k][$k1][$k2] = $t2[0];
                            }
                        }
                    } else {
                        if (is_string($v1)) {
                            $t1 = explode('---', $v1);
                            $tmp[$k][$k1] = $t1[0];
                        }
                    }
                }
            } else {
                if (is_string($v)) {
                    $t = explode('---', $v);
                    $tmp[$k] = $t[0];
                }
            }
        }

        $code = isset($_GET['response_code']) ? $_GET['response_code'] : 735200;

        if (isset($tmp[$code])) {
            return $tmp[$code];
        } else {
            return "no this response code '{$code}'";
        }
    }

    /**
     * @param string    mobilePhone    12345678901
     * @param string    createdAt    1572405213
     * @return bool
     */
    public static function checkCaptchaOld($mobilePhone, $captcha)
    {

        $query = (new \yii\db\Query())
            ->from('z1_captcha t')
            ->select('id')
            ->andFilterWhere([
                'and',
                ['<', 'used_times', \Yii::$app->controller->module->captchaMaxTimes],
                ['=', 'mobile_phone', $mobilePhone],
                ['=', 'code', $captcha],
                ['>', 'created_at', time() - \Yii::$app->controller->module->captchaExpire],
            ]);
        $captcha = $query->one();

        if (!$captcha) {
            return false;
        } else {
            \Yii::$app->db->createCommand()
                ->update('z1_captcha', ['used_times' => new \yii\db\Expression('used_times + 1')], sprintf('id = %d', $captcha['id']))
                ->execute();
            return true;
        }
    }

    /**
     * @param string $str abcdef12345
     * @param int $length 6
     * @return string
     */
    public static function getrandstr($str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890', $length = 6)
    {
        // $str='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        $randStr = str_shuffle($str); //打乱字符串
        $rands = substr($randStr, 0, $length); //substr(string,start,length);返回字符串的一部分
        return $rands;
    }

    /**
     * @return obj
     */
    public static function getUser()
    {
        $token = \Yii::$app->request->get('token');
        return ApiAuthenticator::findIdentityByAccessToken($token);
    }

    /**
     * @param string $mobilePhone 15828271234
     * @return mixed
     */
    public static function sendCaptcha($mobilePhone)
    {
        $components = \Yii::$app->controller->module->smsAndCacheComponents;
        $randStr = ApiHelper::getrandstr('1234567890', 6);

        \Yii::$app->controller->module->setComponents($components);

        $smsResult = \Yii::$app->controller->module->captchaSms->send($mobilePhone, $randStr);
        if ($smsResult !== true) {
            return [
                'code' => "735462",
                'msg' => '发送短信失败',
                'data' => $smsResult,
            ];
        } else {
            \Yii::$app->controller->module->captchaCache->set($mobilePhone, $randStr, 60 * $components['captchaSms']['expire']);
            return true;
        }
    }

    /**
     * @param string $mobilePhone 15828271234
     * @param string $code '123456'
     * @return bool
     */
    public static function checkCaptcha($mobilePhone, $code)
    {
        $components = \Yii::$app->controller->module->smsAndCacheComponents;

        \Yii::$app->controller->module->setComponents($components);

        $oldCode = \Yii::$app->controller->module->captchaCache->get($mobilePhone);

        return $oldCode === $code;
    }

    /**
     * @param object $processingObj
     * @return mixed
     */
    public static function processing($processingObj, $io)
    {
        // the path and query params will geted by queryParams,and the path params will rewrite the query params.
        $input['get'] = Yii::$app->request->queryParams;
        $input['post'] = Yii::$app->request->bodyParams;
        $validatedInput = $processingObj->inputValidate($input);
        if (Helper::isReturning($validatedInput)) {
            return Helper::wrapReturn($validatedInput);
        } else {
            $in2dbData = $processingObj->mappingInput2db($validatedInput);
            $completedData = $processingObj->completeData($in2dbData);

            $completedData = HandlingHelper::before($completedData, $io);
            $handledData = $processingObj->handling($completedData);
            $handledData = HandlingHelper::after($handledData);

            if (Helper::isReturning($handledData)) {
                return Helper::wrapReturn($handledData);
            }

            $db2outData = $processingObj->mappingDb2output($handledData);
            $result = $processingObj->completeResult($db2outData);

            return $result;
        }
    }

    /**
     * @param  array $db2outData completed data form database
     * @return array
     */
    public static function completeResult($db2outData = [])
    {
        $result = [
            'code' => ApiCodeMsg::SUCCESS,
            'msg' => ApiCodeMsg::SUCCESS_MSG,
            'data' => $db2outData,
        ];

        $result = Helper::wrapReturn($result);

        return $result;
    }

    /**
     * @param  string $type     inputValidate
     * @param  string $key      join
     * @return string
     */
    public static function proccessingTemplate($type, $key)
    {
        $templates = require 'ApiProcessingTemplate.php';

        if (isset($templates[$type])) {
            if (isset($templates[$type][$key])) {
                return $templates[$type][$key];
            } else {
                return $templates[$type]['default'];
            }
        }

        return '';
    }

    /**
     * @param   mixed       $content            ['info'=>'It is working']
     * @param   int         $level              1 error,2 warning,4 info
     * @param   int         $string             dev, prod
     * @param   string      $logFileAlias       @app/runtime/apibycnf/debug/tmp.log     yii2 Alias
     * @param   int         $maxLogFiles        3
     * @param   int         $maxFileSize        1024 // in KB
     * @return void
     *
     * usage：
     *      \myzero1\apibycnf\components\rest\BaseApiHelper::debug($content, $level = 2, $logFile = '@app/runtime/apibycnf/info/info.log', $maxLogFiles = 3, $maxFileSize = 1);
     *      \myzero1\apibycnf\components\rest\ApiHelper::debug($content, $level = 2, $logFile = '@app/runtime/apibycnf/info/info.log', $maxLogFiles = 3, $maxFileSize = 1);
     */
    public static function debug($content, $level = 4, $env = 'dev', $logFileAlias = '@app/runtime/apibycnf/debug/tmp.log', $maxLogFiles = 3, $maxFileSize = 1024)
    {
        if (YII_ENV == $env) {
            $content = var_export($content, "true"); //将数组或对象转换字符串格式   $content 数组变量
            $logFile = \Yii::getAlias($logFileAlias);

            $log = new \yii\log\FileTarget();
            $log->logFile = $logFile;
            $log->maxLogFiles = $maxLogFiles;
            $log->maxFileSize = $maxFileSize;
            /*
            [
                [0] => message (mixed, can be a string or some complex data, such as an exception object)
                [1] => level (integer)
                [2] => category (string)
                [3] => timestamp (float, obtained by microtime(true))
                [4] => traces (array, debug backtrace, contains the application code call stacks)
                [5] => memory usage in bytes (int, obtained by memory_get_usage()), available since version 2.0.11.
              ]
            */
            // $log->messages[] = ['' . "\r\n", 4, $content, microtime(true)]; //$content 日志打印内容
            $log->messages[] = [
                "\r\n",
                $level,
                $content,
                microtime(true),
            ];

            $log->export();
        }
    }
}
