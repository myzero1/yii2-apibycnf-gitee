<?php

$template['inputValidate']['join'] = <<<'MYPHP'
        if (empty($input['post']['username']) && empty($input['post']['email']) && empty($input['post']['mobile_phone'])) {
            return [
                'code' => '73544061',
                'msg' => 'username,email,mobile_phone至少填写一个',
                'data' => 'username,email,mobile_phone至少填写一个',
            ];
        }

        return Io::inputValidate($input);
MYPHP;
$template['inputValidate']['upload'] = <<<'MYPHP'
        $input['post']['file'] = 'file placeholder';
        return Io::inputValidate($input);
MYPHP;
$template['inputValidate']['default'] = <<<'MYPHP'
        return Io::inputValidate($input);
MYPHP;

$template['completeData']['index'] = <<<'MYPHP'
        // $in2dbData['updated_at'] = $in2dbData['created_at'] = time();
        // $in2dbData = ApiHelper::inputFilter($in2dbData); // You should comment it, when in search action.
        return $in2dbData;
MYPHP;
$template['completeData']['default'] = <<<'MYPHP'
        $in2dbData['updated_at'] = $in2dbData['created_at'] = time();
        $in2dbData = ApiHelper::inputFilter($in2dbData); // You should comment it, when in search action.
        return $in2dbData;
MYPHP;

$template['handling']['captcha'] = <<<'MYPHP'
        $smsResult = ApiHelper::sendCaptcha($mobile = $completedData['mobile_phone']);

        if ($smsResult !== true) {
            return [
                'code' => "735462",
                'msg' => '发送短信失败',
                'data' => $smsResult,
            ];
        }

        return [
            'code' => "735200",
            'msg' => '发送短信成功',
        ];
MYPHP;
$template['handling']['create'] = <<<'MYPHP'
        $model = new \myzero1\apibycnf\example\models\User(); // according to the current situation

        $model->load($completedData, '');

        $trans = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!($flag = $model->save())) {
                $trans->rollBack();
                return ApiHelper::getModelError($model, ApiCodeMsg::DB_BAD_REQUEST);
            }

            if ($flag) {
                $trans->commit();
            } else {
                $trans->rollBack();
                ApiHelper::throwError('Failed to commint the transaction.', __FILE__, __LINE__);
            }

            return $model->attributes;
        } catch (\Exception $e) {
            $trans->rollBack();
            ApiHelper::throwError('Unknown error.', __FILE__, __LINE__);
        }
MYPHP;
$template['handling']['custom'] = <<<'MYPHP'
        $model = ApiHelper::findModel('\myzero1\apibycnf\example\models\User', $completedData['id']);

        $model->load($completedData, '');

        $trans = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!($flag = $model->save())) {
                $trans->rollBack();
                return ApiHelper::getModelError($model, ApiCodeMsg::DB_BAD_REQUEST);
            }

            if ($flag) {
                $trans->commit();
            } else {
                $trans->rollBack();
                ApiHelper::throwError('Failed to commint the transaction.', __FILE__, __LINE__);
            }

            return $model->attributes;
        } catch (\Exception $e) {
            $trans->rollBack();
            ApiHelper::throwError('Unknown error.', __FILE__, __LINE__);
        }

        /*
        $result = [];

        $query = (new Query())
            ->from('user t')
            // ->groupBy(['t.id'])
            // ->join('INNER JOIN', 'info i', 'i.user_id = t.id')
            ->andFilterWhere([
                'and',
                ['=', 'id', $completedData['id']],
            ]);

        $outFieldNames = [
            't.id as id',
        ];

        $query->select(['1']);
        $result['total'] = intval($query->count());

        $pagination = ApiHelper::getPagination($completedData);
        $query->limit($pagination['page_size']);
        $offset = $pagination['page_size'] * ($pagination['page'] - 1);
        $query->offset($offset);
        $result['page'] = intval($pagination['page']);
        $result['page_size'] = intval($pagination['page_size']);

        // $sortStr = ApiHelper::getArrayVal($completedData, 'sort', '');
        // $sort = ApiHelper::getSort($sortStr, array_keys($outFieldNames), '+id');
        // $query->orderBy([$sort['sortFiled'] => $sort['sort']]);

        $query->select($outFieldNames);

        //  var_dump($query->createCommand()->getRawSql());exit;

        $items = $query->all();
        $result['items'] = $items;

        return $result;
        */

        /*
        $completedData['page_size'] = ApiHelper::EXPORT_PAGE_SIZE;
        $completedData['page'] = ApiHelper::EXPORT_PAGE;

        $index = new Index();
        $items = $index->processing($completedData);

        $exportParams = [
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $items['data']['items'],
            ]),

            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => 'name',
                ],
                [
                    'header' => 'description',
                    'content' => function ($row) {
                        return $row['des'];
                    }
                ],
            ],
        ];

        $name = sprintf('export-%s', time());
        $filenameBase = Yii::getAlias(sprintf('@app/web/%s', $name));

        ApiHelper::createXls($filenameBase, $exportParams);

        return [
            'url' => Yii::$app->urlManager->createAbsoluteUrl([sprintf('/%s.xls', $name)])
        ];
        */
MYPHP;
$template['handling']['delete'] = <<<'MYPHP'
        $model = ApiHelper::findModel('\myzero1\apibycnf\example\models\User', $completedData['id']);

        $model->load($completedData, '');

        $model->is_del = 2;

        $trans = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!($flag = $model->save())) {
                $trans->rollBack();
                return ApiHelper::getModelError($model, ApiCodeMsg::DB_BAD_REQUEST);
            }

            if ($flag) {
                $trans->commit();
            } else {
                $trans->rollBack();
                ApiHelper::throwError('Failed to commint the transaction.', __FILE__, __LINE__);
            }

            return $model->attributes;
        } catch (\Exception $e) {
            $trans->rollBack();
            ApiHelper::throwError('Unknown error.', __FILE__, __LINE__);
        }
MYPHP;
$template['handling']['export'] = <<<'MYPHP'
        $completedData['page_size'] = ApiHelper::EXPORT_PAGE_SIZE;
        $completedData['page'] = ApiHelper::EXPORT_PAGE;

        $index = new Index();
        $items = $index->processing()['data'];

        $exportParams = [
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $items['items'],
            ]),
            /*
            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => 'name',
                ],
                [
                    'header' => 'description',
                    'content' => function ($row) {
                        return $row['des'];
                    }
                ],
            ],
            */
        ];

        $name = sprintf('export-%s', time());
        $filenameBase = Yii::getAlias(sprintf('@app/web/%s', $name));

        ApiHelper::createXls($filenameBase, $exportParams);

        return [
            'url' => Yii::$app->urlManager->createAbsoluteUrl([sprintf('/%s.xls', $name)])
        ];
MYPHP;
$template['handling']['index'] = <<<'MYPHP'
        $result = [];

        $query = (new Query())
            ->from('z1_user t')
            // ->groupBy(['t.id'])
            // ->join('INNER JOIN', 'info i', 'i.user_id = t.id')
            ->andFilterWhere([
                'and',
                ['=', 'username', $completedData['username']],
            ]);

        $outFieldNames = [
            't.id as id',
        ];

        $query->select(['1']);
        $result['total'] = intval($query->count());

        $pagination = ApiHelper::getPagination($completedData);
        $query->limit($pagination['page_size']);
        $offset = $pagination['page_size'] * ($pagination['page'] - 1);
        $query->offset($offset);
        $result['page'] = intval($pagination['page']);
        $result['page_size'] = intval($pagination['page_size']);

        // $sortStr = ApiHelper::getArrayVal($completedData, 'sort', '');
        // $sort = ApiHelper::getSort($sortStr, array_keys($outFieldNames), '+id');
        // $query->orderBy([$sort['sortFiled'] => $sort['sort']]);

        $query->select($outFieldNames);

        //  var_dump($query->createCommand()->getRawSql());exit;

        $items = $query->all();
        $result['items'] = $items;

        return $result;
MYPHP;
$template['handling']['join'] = <<<'MYPHP'
        $model = new \myzero1\apibycnf\components\rest\ApiAuthenticator();

        $model->load($completedData, '');

        // $model->generateAuthKey();
        $model->setPassword($completedData['password']);

        $trans = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!($flag = $model->save())) {
                $trans->rollBack();
                return ApiHelper::getModelError($model, ApiCodeMsg::INTERNAL_SERVER);
            }

            if (isset($completedData['mobile_phone']) && isset($completedData['captcha'])) {
                if (true !== ApiHelper::checkCaptcha($completedData['mobile_phone'], $completedData['captcha'])) {
                    return [
                        'code' => "735465",
                        'msg' => '验证码错误',
                        'data' => '验证码错误',
                    ];
                }
            }

            if ($flag) {
                $trans->commit();
            } else {
                $trans->rollBack();
                throw new ServerErrorHttpException('Failed to save commit reason.');
            }

            return $model->attributes;
        } catch (\Exception $e) {
            $trans->rollBack();
            throw new ServerErrorHttpException('Failed to save all models reason.');
        }
MYPHP;
$template['handling']['login'] = <<<'MYPHP'
        $model = \myzero1\apibycnf\components\rest\ApiAuthenticator::findByUsername($completedData['username']);

        if (is_null($model)) {
            return [
                'code' => "735461",
                'msg' => '用户名或密码错误',
            ];
        }

        if ($completedData['type'] == 1) {
            if (!isset($completedData['captcha']) || true !== ApiHelper::checkCaptcha($completedData['username'], $completedData['captcha'])) {
                return [
                    'code' => "735465",
                    'msg' => '验证码错误',
                    'data' => '验证码错误',
                ];
            }
        } else {
            if (!isset($completedData['password']) || !$model->validatePassword($completedData['password'], $model->password_hash)) {
                return [
                    'code' => "735461",
                    'msg' => '用户名或密码错误',
                    'data' => '用户名或密码错误',
                ];
            }
        }

        if (!\myzero1\apibycnf\components\rest\ApiAuthenticator::apiTokenIsValid($model->api_token)) {
            $model->generateApiToken();
        }

        $trans = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!($flag = $model->save())) {
                $trans->rollBack();
                return ApiHelper::getModelError($model, ApiCodeMsg::INTERNAL_SERVER);
            }

            if ($flag) {
                $trans->commit();
            } else {
                $trans->rollBack();
                throw new ServerErrorHttpException('Failed to save commit reason.');
            }

            return $model->attributes;
        } catch (\Exception $e) {
            $trans->rollBack();
            throw new ServerErrorHttpException('Failed to save all models reason.');
        }
MYPHP;
$template['handling']['update'] = <<<'MYPHP'
        $model = ApiHelper::findModel('\myzero1\apibycnf\example\models\User', $completedData['id']);

        $model->load($completedData, '');

        $trans = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!($flag = $model->save())) {
                $trans->rollBack();
                return ApiHelper::getModelError($model, ApiCodeMsg::DB_BAD_REQUEST);
            }

            if ($flag) {
                $trans->commit();
            } else {
                $trans->rollBack();
                ApiHelper::throwError('Failed to commint the transaction.', __FILE__, __LINE__);
            }

            return $model->attributes;
        } catch (\Exception $e) {
            $trans->rollBack();
            ApiHelper::throwError('Unknown error.', __FILE__, __LINE__);
        }
MYPHP;
$template['handling']['upload'] = <<<'MYPHP'
        $file = \yii\web\UploadedFile::getInstanceByName('file');
        $newDirectory = $completedData['directory'];
        \yii\helpers\BaseFileHelper::createDirectory($newDirectory);
        $newName = sprintf('%s/%s-%s.%s', $newDirectory, $file->baseName, time(), $file->extension);
        $extensions = explode(',', $completedData['extension']);
        if (!in_array($file->extension, $extensions)) {
            return [
                'code' => '735400',
                'msg' => '输入参数验证错误',
                'data' => [
                    "file" => [
                        sprintf('只允许上传后缀为%s的文件', $completedData['extension'])
                    ]
                ],
            ];
        }

        $ignore = sprintf('%s/.gitignore', $newDirectory);

        file_put_contents($ignore, "*\n!.gitignore");
        $file->saveAs($newName);

        return [
            'absUrl' => Yii::$app->urlManager->createAbsoluteUrl([$newName]),
            'relUrl' => '/' . $newName,
        ];
MYPHP;
$template['handling']['view'] = <<<'MYPHP'
        $result = (new Query())
            ->from('z1_user t')
            // ->groupBy(['t.id'])
            // ->join('INNER JOIN', 'info i', 'i.user_id = t.id')
            ->andFilterWhere([
                'and',
                ['=', 't.id', $completedData['id']],
            ])
            ->select([
                't.id',
            ])
            ->one();

        if (!$result) {
            $result = [
                'code' => ApiCodeMsg::NOT_FOUND,
                'msg' => ApiCodeMsg::NOT_FOUND_MSG,
                'data' => new \StdClass(),
            ];
        }

        return $result;
MYPHP;
$template['handling']['default'] = <<<'MYPHP'
        $model = ApiHelper::findModel('\myzero1\apibycnf\example\models\User', $completedData['id']);

        $model->load($completedData, '');

        $trans = Yii::$app->db->beginTransaction();
        try {
            $flag = true;
            if (!($flag = $model->save())) {
                $trans->rollBack();
                return ApiHelper::getModelError($model, ApiCodeMsg::DB_BAD_REQUEST);
            }

            if ($flag) {
                $trans->commit();
            } else {
                $trans->rollBack();
                ApiHelper::throwError('Failed to commint the transaction.', __FILE__, __LINE__);
            }

            return $model->attributes;
        } catch (\Exception $e) {
            $trans->rollBack();
            ApiHelper::throwError('Unknown error.', __FILE__, __LINE__);
        }

        /*
        $result = [];

        $query = (new Query())
            ->from('user t')
            // ->groupBy(['t.id'])
            // ->join('INNER JOIN', 'info i', 'i.user_id = t.id')
            ->andFilterWhere([
                'and',
                ['=', 'id', $completedData['id']],
            ]);

        $outFieldNames = [
            't.id as id',
        ];

        $query->select(['1']);
        $result['total'] = intval($query->count());

        $pagination = ApiHelper::getPagination($completedData);
        $query->limit($pagination['page_size']);
        $offset = $pagination['page_size'] * ($pagination['page'] - 1);
        $query->offset($offset);
        $result['page'] = intval($pagination['page']);
        $result['page_size'] = intval($pagination['page_size']);

        // $sortStr = ApiHelper::getArrayVal($completedData, 'sort', '');
        // $sort = ApiHelper::getSort($sortStr, array_keys($outFieldNames), '+id');
        // $query->orderBy([$sort['sortFiled'] => $sort['sort']]);

        $query->select($outFieldNames);

        //  var_dump($query->createCommand()->getRawSql());exit;

        $items = $query->all();
        $result['items'] = $items;

        return $result;
        */

        /*
        $completedData['page_size'] = ApiHelper::EXPORT_PAGE_SIZE;
        $completedData['page'] = ApiHelper::EXPORT_PAGE;

        $index = new Index();
        $items = $index->processing($completedData);

        $exportParams = [
            'dataProvider' => new \yii\data\ArrayDataProvider([
                'allModels' => $items['data']['items'],
            ]),

            'columns' => [
                [
                    'attribute' => 'name',
                    'label' => 'name',
                ],
                [
                    'header' => 'description',
                    'content' => function ($row) {
                        return $row['des'];
                    }
                ],
            ],
        ];

        $name = sprintf('export-%s', time());
        $filenameBase = Yii::getAlias(sprintf('@app/web/%s', $name));

        ApiHelper::createXls($filenameBase, $exportParams);

        return [
            'url' => Yii::$app->urlManager->createAbsoluteUrl([sprintf('/%s.xls', $name)])
        ];
        */
MYPHP;

$template['mappingDb2output']['index'] = <<<'MYPHP'
        $outputFieldMap = [
            'name735' => 'demo_name',
            'description735' => 'demo_description',
        ];

        $db2outData = $handledData;

        foreach ($db2outData['items'] as $k => $v) {
            $db2outData['items'][$k] = ApiHelper::db2OutputField($db2outData['items'][$k], $outputFieldMap);

            // $db2outData['items'][$k]['created_at'] = ApiHelper::time2string($db2outData['items'][$k]['created_at']);
            // $db2outData['items'][$k]['updated_at'] = ApiHelper::time2string($db2outData['items'][$k]['updated_at']);
        }

        return $db2outData;
MYPHP;
$template['mappingDb2output']['default'] = <<<'MYPHP'
        $outputFieldMap = [
            'name735' => 'demo_name',
            'description735' => 'demo_description',
        ];
        $db2outData = ApiHelper::db2OutputField($handledData, $outputFieldMap);

        // $db2outData['created_at'] = ApiHelper::time2string($db2outData['created_at']);
        // $db2outData['updated_at'] = ApiHelper::time2string($db2outData['updated_at']);

        return $db2outData;
MYPHP;

$template['handling']['holdon'] = <<<'MYPHP'

MYPHP;

return $template;
