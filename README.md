# yii2-apibycnf

You can generate restfull api by configuration.

Base on [`yii2-apibycnf`](https://github.com/myzero1/yii2-apibycnf)

## Show time

![](https://gitee.com/myzero1/show-time/raw/master/yii2-apibycnf-gitee/101.png)

![](https://gitee.com/myzero1/show-time/raw/master/yii2-apibycnf-gitee/102.png)

![](https://gitee.com/myzero1/show-time/raw/master/yii2-apibycnf-gitee/103.png)

![](https://gitee.com/myzero1/show-time/raw/master/yii2-apibycnf-gitee/104.png)

## Installation

The preferred way to install this module is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require myzero1/yii2-apibycnf-gitee
```

or add

```
"myzero1/yii2-apibycnf-gitee": "*"
```

to the require section of your `composer.json` file.

## Setting

Once the extension is installed, simply modify your application configuration as follows:

`In main-local.php`

```php
...
if (!YII_ENV_TEST) {
    // configuration adjustments for 'dev' environment
    ...
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
    $config['modules']['gii']['generators'] = [
        'api' => [
            'class' => 'myzero1\apibycnf\components\gii\generators\rest\Generator'
        ],
    ];
    ...
}
...
```

`In main.php`

```php
return [
    ......
    'bootstrap' => [
        ......
        'example',
        ......
    ],
    ......
    'modules' => [
        ......
        'apibycnf' => 'myzero1\apibycnf\Module',
        'example' => [
            'class' => 'myzero1\apibycnf\example\ApiByCnfModule', // should add table to db by 'yii2-apibycnf/src/user.sql'
            'apiTokenExpire' => 24 * 3600 * 365,
            'fixedUser' => [
                'id' => '1',
                'username' => 'myzero1',
                'api_token' => 'myzero1Token',
            ],
            'smsAndCacheComponents' => [
                'captchaCache' => [
                    'class' => '\yii\caching\FileCache',
                    'cachePath' => '@runtime/captchaCache',
                ],
                'captchaSms' => [
                    'class' => 'myzero1\smser\QcloudsmsSmser', // 腾讯云
                    'appid' => '1400280810', // 请替换成您的appid
                    'appkey' => '23e167badfc804d97d454e32e258b780', // 请替换成您的apikey
                    'smsSign' => '玩索得',
                    'expire' => '5', //分钟
                    'templateId' => 459670, // 请替换成您的templateId
                ],
            ],
            'runningAsDocActions' => [
                '*' => '*', // all ations, as default
                // 'controllerA' => [
                //     '*', // all actons in controllerA
                // ],
                // 'controllerB' => [
                //     'actionB',
                // ],
            ],
        ],
        ......
    ],
    ......
    'components' => [
        ......
        // 'assetManager' => [
        //     'class' => 'yii\web\AssetManager',
        //     'forceCopy' => true, // true/false
        // ],
        'user' => [
            // 'identityClass' => 'myzero1\apibycnf\components\rest\ApiAuthenticator',
            'identityClass' => 'myzero1\apibycnf\example\components\MyApiAuthenticator', // Rewrite
            'enableSession' => false,
            'authTimeout' => 3600 * 24, // defafult 24h
        ],
        ......
    ]
    ......
];
```

## Usage

### Selectable modules

- Set basePath to "/v2"
- Click the "Preview" button
- Click the "Generate" button, to Generate the codes.
- Set the config files

`In main.php`

```php
return [
    ......
    'bootstrap' => [
        ......
        'v2',
        ......
    ],
    ......
    'modules' => [
        ......
        'v2' => [
            'class' => 'backend\modules\v2\ApiByCnfModule',
            'apiTokenExpire' => 24 * 3600 * 365,
            'fixedUser' => [
                'id' => '1',
                'username' => 'myzero1',
                'api_token' => 'myzero1Token',
            ],
            'smsAndCacheComponents' => [
                'captchaCache' => [
                    'class' => '\yii\caching\FileCache',
                    'cachePath' => '@runtime/captchaCache',
                ],
                'captchaSms' => [
                    'class' => 'myzero1\smser\QcloudsmsSmser', // 腾讯云
                    'appid' => '1400280810', // 请替换成您的appid
                    'appkey' => '23e167badfc804d97d454e32e258b780', // 请替换成您的apikey
                    'smsSign' => '玩索得',
                    'expire' => '5', //分钟
                    'templateId' => 459670, // 请替换成您的templateId
                ],
            ],
            'runningAsDocActions' => [
                '*' => '*', // all ations, as default
                // 'controllerA' => [
                //     '*', // all actons in controllerA
                // ],
                // 'controllerB' => [
                //     'actionB',
                // ],
            ],
        ],
        ......
    ],
    ......
];
```

- the `v2` will display to Selectable modules menu as `v2 api`
- you can click the `v2 api` button to config the `v2`
- you can add `response_code` param to return characteristic return

### The other menu of restbyconfig

- you can click the `Swagger` button to use it.
- you can click the `Markdown` button to use it.

### Can overwrite classes

- myzero1\apibycnf\components\rest\Helper
- myzero1\apibycnf\components\rest\ApiHelper
- myzero1\apibycnf\components\rest\ApiAuthenticator
- myzero1\apibycnf\components\rest\HandlingHelper

`In main.php`

```php
return [
    ......
    'bootstrap' => [
        ......
        'classMap' => function(){
             Yii::$classMap['myzero1\apibycnf\components\rest\Helper'] = '@app/modules/v1/components/Helper.php';
             Yii::$classMap['myzero1\apibycnf\components\rest\ApiHelper'] = 'path/to/ApiHelper.php';
        },
        ......
    ],
    ......
];
```

### notice

- 设置 git 避免，文件类型问题导致的换行符问题
- 提交时转换为 LF，检出时不转换 git config --global core.autocrlf input
- 拒绝提交包含混合换行符的文件 git config --global core.safecrlf true
- git-windows 自带有 dos2unix.exe, 执行 find . -type f -exec dos2unix {} \; 批量转换
- 使用这个方便输出接收到的参数，file_put_contents(\Yii::getAlias('@runtime/captcha'), json_encode(\$input, JSON_UNESCAPED_UNICODE + JSON_PRETTY_PRINT) . "\n", FILE_APPEND);
- \myzero1\apibycnf\components\rest\ApiHelper::debug('It just a testing', $level = 2, $env = 'dev', $logFile = '@app/runtime/apibycnf/info/info.log', $maxLogFiles = 3, \$maxFileSize = 1);

### change logs

- Add my group
- Carding code
- Multiple responses

### TODO
